package com.day18task3.repository;

import com.day18task3.model.Student;
import com.day18task3.model.StudentSubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class JdbcStudentSubjectRepository implements StudentSubjectRepository {

    // Spring Boot will create and configure DataSource and JdbcTemplate
    // To use it, just @Autowired
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public int count(int studentid) {
        return jdbcTemplate
                .queryForObject("select count(*) from studentsubject", Integer.class);
    }

    @Override
    public int insert(StudentSubject studentsubject) {
        return jdbcTemplate.update(
                "insert into studentsubject (studentid, subjectid, attendance, score) values(?,?,?,?)",
                studentsubject.getStudentid(), studentsubject.getSubjectid(), "0000-00-00", 0);
    }

    @Override
    public int updateAttendance(int id, String attendance) {
        return jdbcTemplate.update(
                "update studentsubject set attendance = ? where id = ?",
                attendance, id);
    }

    @Override
    public int deleteById(int id) {
        return jdbcTemplate.update(
                "delete from studentsubject where id = ?",
                id);
    }

    @Override
    public Optional<StudentSubject> findById(int id) {
        return jdbcTemplate.queryForObject(
                "select * from studentsubject where id = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        Optional.of(new StudentSubject(
                                rs.getInt("id"),
                                rs.getInt("studentid"),
                                rs.getInt("subjectid"),
                                rs.getString("attendance"),
                                rs.getInt("score")
                        ))
        );
    }

    @Override
    public List<StudentSubject> findAll() {
        return jdbcTemplate.query(
                "select * from studentsubject",
                (rs, rowNum) ->
                        new StudentSubject(
                                rs.getInt("id"),
                                rs.getInt("studentid"),
                                rs.getInt("subjectid"),
                                rs.getString("attendance"),
                                rs.getInt("score")
                        )
        );
    }

    @Override
    public List<StudentSubject> findByStudentId(int studentid) {
        return jdbcTemplate.query(
                "select * from studentsubject where studentid = ?",
                new Object[]{studentid},
                (rs, rowNum) ->
                        new StudentSubject(
                                rs.getInt("id"),
                                rs.getInt("studentid"),
                                rs.getInt("subjectid"),
                                rs.getString("attendance"),
                                rs.getInt("score")
                        )
        );
    }

    @Override
    public List<StudentSubject> findBySubjectId(int subjectid) {
        return jdbcTemplate.query(
                "select * from studentsubject where subjectid = ?",
                new Object[]{subjectid},
                (rs, rowNum) ->
                        new StudentSubject(
                                rs.getInt("id"),
                                rs.getInt("studentid"),
                                rs.getInt("subjectid"),
                                rs.getString("attendance"),
                                rs.getInt("score")
                        )
        );
    }
}
